# Геосервис

## Основные ссылки на все, что было использовано

1. https://www.openstreetmap.org - основной сайт, откуда скачивать данные по карте
2. https://nominatim.org/release-docs/4.1.0/ - документация по nominatim
3. https://download.geofabrik.de/ - сайт, откуда скачивать готовые полигоны (.pbf) и откуда получать обновления (REPLICATION)
4. https://github.com/mediagis/nominatim-docker/ - докер контейнер, с nominatim
   1. https://github.com/mediagis/nominatim-docker/tree/master/4.1 - версия nominatim, которая используется в сервисе
   2. https://github.com/mediagis/nominatim-docker/issues/245#issuecomment-1072205751 - как использовать EXTERNAL базу данных
5. https://github.com/komoot/photon/ - поисковый движок для реверсивного и прямого геокодирования
6. https://photon.komoot.io/ - обзор, как работает поисковый движок
   1. https://download1.graphhopper.com/public/ - где хранятся ВСЕ поисковые индексы
7. https://project-osrm.org/ - РОУТИНГ. Для посторения дорог между точками
   1. https://project-osrm.org/docs/v5.24.0/api/# - документация по роутингу
8. https://hub.docker.com/r/osrm/osrm-backend/ - docker образ для роутинга (Серверная часть)
9.  https://github.com/Project-OSRM/osrm-backend - основной git OSRM (роутинг)
10. https://github.com/project-osrm/osrm-backend/pkgs/container/osrm-backend - Исходники докер контейнера для OSRM
11. https://hub.docker.com/r/postgis/postgis - extension for PostgreSQL. Для работы с ГеоДанными (Point, Polygon)
12. https://export.hotosm.org - OpenSource система для EXPORT частей карты OSM
13. http://osm-analytics.org - сайт для проведения анализа частотности обновления того или иного полигона на карте
14. https://wiki.openstreetmap.org/wiki/Overpass_API - инструмент для экспортирования КУСКОВ карты
15. https://dev.overpass-api.de/overpass-doc/en/index.html - документация по использованию сервиса OVERPASS
16. https://hub.docker.com/r/wiktorn/overpass-api - запуск сервиса OVERPASS на своих мощностях
17. https://manpages.ubuntu.com/manpages/trusty/man1/osmconvert.1.html - как работает утилита OSMCONVERT (преобразование разных формтов)

## Основные составные части сервиса

- OSM
- OSRM
- PHOTON
- POSTGIS
- BACEND
- ADMIN

### OSM

Зона ответственности

1. EXTRACT файла .osm.pbf -> postgres DB.
2. Обновление существующей БД из переменной REPLICATION_URL

Состоит из

1. nominatim - сервис, который отвечает за все махинации с файлом .osm.pbf
2. postgres - БД, где хранятся готовые данные

### OSRM

Зона ответственности

1. EXTRACT файла .osm.pbf -> Файлы, пригодные для построения РОУТИНГА

Состоит из

1. osrm-tool и остальные

### PHOTON

Зона ответственности

1. EXTRACT данных из БД (OSM) -> ELK DB
2. Прямое геокодирование
3. Обратное геокодирование

Состоит из

1.  photon.jar файл - сам поисковый движок
2.  ELK

## Какие порты EXPOSE из каждого контейнера

| Контейнер | Сервис           | Порт |
| --------- | ---------------- | ---- |
| OSM       | nominatim        | 8080 |
| OSM       | postgres         | 5432 |
| OSRM      | роутинг          | 5000 |
| TILES     | получение tile   | 80   |
| TILES     | DB               | 5432 |
| PHOTON    | поисковый движок | 2322 |
| POSTGIS   | DB               | 5432 |
| BACEND    | NodeJS           | 3000 |
| ADMIN     | react            | 3000 |

## env переменные

Для корректной работы проекта требуется создание .env файла

| Переменная                     | Описание                                                                                     |
| ------------------------------ | -------------------------------------------------------------------------------------------- |
| GS_OSM_DB_PORT                 | Порт БД nominatim (Куда производится import .pbf файла для /reverse, /search, /autocomplete) |
| GS_OSM_DB_PASSWORD             | пароль к БД nominatim                                                                        |
| GS_OSM_NOMINATIM_PORT          | Порт самого nominatim                                                                        |
| GS_OSM_REPLICATION_URL         | URL, откуда будут браться РЕПЛИКИ для БД nominatim                                           |
| GS_OSRM_PORT                   | Порт движка OSRM (Построение дорог)                                                          |
| GS_TILES_PORT                  | Порт движка TileServer (Получение тайлов для карты - Leaflet)                                |
| GS_TILES_DB_PORT               | Порт TileServer_DB                                                                           |
| GS_PHOTON_PORT                 | Порт движка Photon (отвечает за )                                                            |
| GS_PHOTON_VERSION              | Версия photon                                                                                |
| GS_POSTGIS_BACKEND_PORT        | Порт БД, где мы будем хранить дополнительные адреса и ТД                                     |
| GS_POSTGIS_BACKEND_DB_NAME     | Название БД                                                                                  |
| GS_POSTGIS_BACKEND_DB_USER     | имя пользователя в БД                                                                        |
| GS_POSTGIS_BACKEND_DB_PASSWORD | пароль от БД                                                                                 |
| GS_BACKEND_PORT                | Порт сервера, с которым будет работать пользователь                                          |
| GS_WEB_ADMIN_PORT              | Порт, для доступа к админке                                                                  |
| GS_WEB_ADMIN_BACK_URL          | URL, куда будет обращаться АДМИНКА. Пример: http://localhost:4000                            |

### Данные по координатам - для загрузки кастомного полигона

| Переменная    | Описание              |
| ------------- | --------------------- |
| GS_LEFT_MAP   | Левая сторона карты   |
| GS_BOTTOM_MAP | Нижняя сторона карты  |
| GS_RIGHT_MAP  | Правый край карты     |
| GS_TOP_MAP    | Верхняя сторона карты |

### URL для прямой загрузки

| Переменная | Описание                           |
| ---------- | ---------------------------------- |
| GS_PBF_URL | прямая ссылка на скачку файла .pbf |

### Дополнительные переменные

| Переменная    | Описание                                                          |
| ------------- | ----------------------------------------------------------------- |
| GS_DOCKER_NET | название сети докеров, к которой будут приконнечны все контейнеры |

## Подготовка к запуску

Что должно быть установлено на системе

1. docker (не только установлен, но и запущен)
2. docker-compose
3. git
4. curl

Создать файл .env (В корневой директории проекта, на одном уровне с файлом .env.example)

1. Все поля из файла .env.example - скопировать в новый файл .env
2. Без этого файла - запуститься не получится

Выбор зоны, с которой будет работать сервис. Файлы для карты карты можно скачать двумя вариантами

1.  Готовая область
2.  Специфическая зона на карте

**Готовая область**

1. Зайти на сайт - https://download.geofabrik.de/
2. Выбрать необходимый регион
3. Скопировать ПРЯМУЮ ссылку на его скачивание и вставить ее в файл .env в соответствующее поле 1. GS_PBF_URL

**Специфическая зона на карте**

1. Зайти на сайт - https://www.openstreetmap.org
2. Клик на кнопку "Экспорт" и Выбираем нужный регион
   1. Либо - ВСЯ карта на экране
   2. Либо - Выделить другую область
3. В левом верхнем углу будут 4 координаты. (LEFT, TOP, RIGHT, BOTTOM)
4. Эти координаты необходимо скопировать в файл .env в соответствующие поля
   1. GS_LEFT_MAP - Левая сторона карты
   2. GS_BOTTOM_MAP - Нижняя сторона карты
   3. GS_RIGHT_MAP - Правый край карты
   4. GS_TOP_MAP - Верхняя сторона карты
5. На основе выбранной зоны - необзодимо выбрать REPLICAtiON_URL

## Порядок запуска

### Запуск скачки файлов и их подготовки

Если это кастомная область (По 4ем координатам)

```
./init.sh map-data-custom-area
```

Если это прямая ссылка на карту

```
./init.sh map-data-pbf-url
```

### Запуск OSM (nominatim)

```
./init.sh osm
```

### Запуск PHOTON

Скачка самой библиотеки PHOTON

```
./init.sh photon-download
```

Импортирование данных для PHOTON в ELK

```
./init.sh photon-import
```

Запуск самого PHOTON

```
./init.sh photon
```

### Запуск OSRM

Подготовка файлов для работы OSRM

```
./init.sh osrm-extract
```

Запуск самого OSRM

```
./init.sh osrm
```

### Запуск TileServer

Подготовка файлов для работы TileServer

```
./init.sh tile-server-import
```

Запуск самого TileServer

```
./init.sh tile-server
```

### Запуск POSTGIS

```
./init.sh postgis
```

### Запуск BACK

```
./init.sh back
```

### Запуск ADMIN

```
./init.sh admin
```

## Полная остановка всех контейнеров

```
./init.sh stop-all
```

## Полная остановка всех контейнеров + очистка системы (Удаление всех файлов)

```
./init.sh stop-and-clear
```

## Проверка работоспособности сервиса

| Сервис | URL                                                                                            | Комментарий        |
| ------ | ---------------------------------------------------------------------------------------------- | ------------------ |
| OSM    | http://localhost:OSM_PORT/reverse?lat=10&lon=10&format=json                                    | Использовать curl  |
| PHOTON | http://localhost:PHOTON_PORT/api?q=kekis&limit=10                                              | Использовать curl  |
| OSRM   | http://localhost:OSRM_PORT/route/v1/driving/40.536184,64.553385;40.545104,64.544934?steps=true | Использовать curl  |
| BACK   | http://localhost:BACK_PORT/test                                                                | Использовать curl  |
| ADMIN  | http://localhost:ADMIN_PORT                                                                    | Открыть в браузере |

## Полезные команды

1. Скачать photon - wget https://github.com/komoot/photon/releases/download/0.4.1/photon-0.4.1.jar -O /map-files/photon-0.4.1.jar
2. Вызвать обновление PHOTON - curl http://localhost:2322/nominatim-update
3. Вызвать обновление NOMINATIM - docker exec -it nominatim sudo -u nominatim nominatim replication --project-dir /nominatim
4. host.docker.internal - как докеру обращаться к хостовой машине
5. https://download.geofabrik.de/russia/northwestern-fed-district-updates/ - пример REPLICATION_URL
6. https://download.geofabrik.de/russia/crimean-fed-district-latest.osm.pbf - пример прямого URL для скачки PBF
7. /usr/lib/postgresql/14/bin/pg_ctl -D /var/lib/postgresql/14/main -l logfile start - ХЗ что это...
8. ERROR: Input data is not ordered: relation id 15019381 appears more than once
   1. Причина ошибки: в ИСХОДНОМ .osm файле в самом конце есть вот такая вот надпись <remark> runtime error: Query timed out in "print" at line 1 after 210 seconds. </remark>
   2. Когда производится попытка СКАЧАТЬ кусок карты с overpass-api - там есть ДЕФОЛТНЫЙ таймаут и равняется он 210 sec
   3. чтобы увеличить таймаут нужно добавить в запрос поле: [timeout:1600];
