#!/bin/bash

#Export ENV vars
if [ ! -f .env ]
then
    echo "You need to create .env file"
    exit 1
fi

export $(cat .env | xargs)

#Основные момент запуска
function mapDataCustomArea () {
    echo "start download .osm file by COORDINATES"

    # Удаляем содержимое в папке с загружаемым полигоном
    echo "remove file ./map-files/map.osm"
    rm -f ./map-files/map.osm

    echo "remove file ./map-files/map.osm.pbf"
    rm -f ./map-files/map.osm.pbf

    # Скачиваем полигон с overpassapi
    # URL="https://overpass-api.de/api/map?bbox="$GS_LEFT_MAP,$GS_BOTTOM_MAP,$GS_RIGHT_MAP,$GS_TOP_MAP
    # 64.8489 39.1718 | 64.0962 39.1718 | 64.0962 41.9266 | 64.8489 41.9266 | 64.8489 39.1718
    POLY="$GS_TOP_MAP+|+$GS_LEFT_MAP+|+$GS_BOTTOM_MAP+|+$GS_LEFT_MAP+|+$GS_BOTTOM_MAP+|+$GS_RIGHT_MAP+|+$GS_TOP_MAP+|+$GS_RIGHT_MAP+|+$GS_TOP_MAP+|+$GS_LEFT_MAP"
    echo "POLY =" $POLY

    #Скачиваем полигон с overpassapi
    #Конвертация карты в .osm -> .osm.pbf
    docker run -it --rm \
        -v ${PWD}/map-files:/map-files \
        -v ${PWD}/presets:/presets \
        -e POLY=$POLY \
        --add-host=host.docker.internal:host-gateway \
        --name mapDataCustomArea \
        ubuntu:latest \
        /bin/sh -c \
            'apt update -y && \
            apt install curl -y && \
            apt install osmctools -y && \
            sed -E "s/POLY_DATA_HERE/$POLY/g" /presets/overpass-api-req.txt > /overpass-api-req-with-plus.txt && \
            sed -E "s/\+\|\+/ /g" /overpass-api-req-with-plus.txt > /overpass-api-req-clear.txt && \
            URL_DATA=`cat /overpass-api-req-clear.txt` && \
            curl -o /map-files/map.osm -d "data=$URL_DATA" -H "Content-Type: application/x-www-form-urlencoded" -X POST https://overpass-api.de/api/interpreter && \
            osmconvert /map-files/map.osm --out-pbf -o=/map-files/map.osm.pbf'

    chmod +r ./map-files/map.osm.pbf
}

function mapDataPbfUrl () {
    echo "start download .pbf file by URL"

    # Удаляем содержимое в папке с загружаемым полигоном
    echo "remove file ./map-files/map.osm"
    rm -f ./map-files/map.osm

    echo "remove file ./map-files/map.osm.pbf"
    rm -f ./map-files/map.osm.pbf

    #Скачиваем полигон с overpassapi
    #Конвертация карты в .osm -> .osm.pbf
    docker run -it --rm \
        -v ${PWD}/map-files:/map-files \
        -e MAP_URL=$URL \
        --name mapDataPbfUrl \
        ubuntu:latest \
        /bin/sh -c \
            'apt update -y && \
            apt install wget -y && \
            wget $MAP_URL -O /map-files/map.osm.pbf'

    chmod +r ./map-files/map.osm.pbf
}

# Запуск NOMINATIM для импорта в БД
function osm () {
    echo "start OSM"

    FILE=dc-osm.yaml
    
    echo "DOWN OSM docker container"
    docker-compose -f $FILE down
    
    echo "UP OSM docker container"
    docker-compose -f $FILE up -d

    echo "LOGS OSM"
    docker-compose -f $FILE logs -f --tail 100
}

# Скачать Photon
function photonDownload () {
    echo "PHOTON download"
    docker run -it --rm \
        -v ${PWD}/map-files:/map-files \
        -e GS_PHOTON_VERSION=$GS_PHOTON_VERSION \
        --add-host=host.docker.internal:host-gateway \
        --name photonDownload \
        ubuntu:latest \
        /bin/sh -c \
            'apt update -y && \
            apt install wget -y && \
            wget https://github.com/komoot/photon/releases/download/$GS_PHOTON_VERSION/photon-$GS_PHOTON_VERSION.jar -O /map-files/photon-$GS_PHOTON_VERSION.jar'
}

# Импорт данных в PHOTON
function photonImport () {
    echo "PHOTON import"
    docker run -it --rm \
        -v ${PWD}/map-files:/map-files \
        -v ${PWD}/photon-data:/photon_data \
        -e GS_OSM_DB_PORT=$GS_OSM_DB_PORT \
        -e GS_OSM_DB_PASSWORD=$GS_OSM_DB_PASSWORD \
        -e GS_PHOTON_VERSION=$GS_PHOTON_VERSION \
        --add-host=host.docker.internal:host-gateway \
        --name photonImport \
        openjdk:18-alpine \
        /bin/sh -c \
            'apk update && \
            java -jar /map-files/photon-$GS_PHOTON_VERSION.jar -nominatim-import -host host.docker.internal -port $GS_OSM_DB_PORT -database nominatim -user nominatim -password $GS_OSM_DB_PASSWORD'
}

# Запуск photon + БД
function photon () {
    echo "start PHOTON"

    FILE=dc-photon.yaml

    echo "DOWN PHOTON docker container"
    docker-compose -f $FILE down
    
    echo "UP PHOTON docker container"
    docker-compose -f $FILE up -d

    echo "LOGS PHOTON"
    docker-compose -f $FILE logs -f --tail 100
}

# Подготовка файла osm -> osrm
function osrmExtract () {
    echo "OSRM extract"
    docker run -it --rm \
        -v ${PWD}/map-files:/map-files \
        -e GS_OSRM_VERSION=$GS_OSRM_VERSION \
        --name osrmExtract \
        ghcr.io/project-osrm/osrm-backend:$GS_OSRM_VERSION \
        osrm-extract -p /opt/car.lua /map-files/map.osm.pbf

    echo "OSRM partition"
    docker run -it --rm \
        -v ${PWD}/map-files:/map-files \
        -e GS_OSRM_VERSION=$GS_OSRM_VERSION \
        --name osrmPartition \
        ghcr.io/project-osrm/osrm-backend:$GS_OSRM_VERSION \
        osrm-partition /map-files/map.osrm

    echo "OSRM customize"
    docker run -it --rm \
        -v ${PWD}/map-files:/map-files \
        -e GS_OSRM_VERSION=$GS_OSRM_VERSION \
        --name osrmCustomize \
        ghcr.io/project-osrm/osrm-backend:$GS_OSRM_VERSION \
        osrm-customize /map-files/map.osrm
}

# osrm
function osrm () {
    echo "start OSRM"

    FILE=dc-osrm.yaml

    echo "DOWN OSRM docker container"
    docker-compose -f $FILE down
    
    echo "UP OSRM docker container"
    docker-compose -f $FILE up -d

    echo "LOGS OSRM"
    docker-compose -f $FILE logs -f --tail 100
}

function tileServerImport () {
    echo "TileServerImport"
    docker run -it --rm \
        -v ${PWD}/map-files/map.osm.pbf:/data/region.osm.pbf \
        -v ${PWD}/tile-server-database:/data/database \
        --name tileServerImport \
        overv/openstreetmap-tile-server \
        import
}

function tileServer () {
    echo "TileServer"

    echo "mkdir tile-server-tiles"
    mkdir tile-server-tiles

    echo "chmod ./tile-server-tiles"
    chmod -R 777 ./tile-server-tiles

    FILE=dc-tile-server.yaml

    echo "DOWN TILE_SERVER docker container"
    docker-compose -f $FILE down
    
    echo "UP TILE_SERVER docker container"
    docker-compose -f $FILE up -d

    echo "LOGS TILE_SERVER"
    docker-compose -f $FILE logs -f --tail 100
}

# Postgis DB
function postgis () {
    echo "start POSTGIS DB"

    FILE=dc-postgis.yaml

    echo "DOWN POSTGIS docker container"
    docker-compose -f $FILE down
    
    echo "UP POSTGIS docker container"
    docker-compose -f $FILE up -d

    echo "LOGS POSTGIS"
    docker-compose -f $FILE logs -f --tail 100
}

# Back-API
function back () {
    echo "start back app"

    echo "remove folder ./geo-service-back"
    rm -Rf ./geo-service-back

    echo "back clone"
    git clone https://gitlab.com/zibellon-services/geo-service-back.git

    echo "BUILD BACK docker container"
    docker build ./geo-service-back -t geo-service-back:latest

    echo "RM folder"
    rm -Rf ./geo-service-back

    FILE=dc-back.yaml

    echo "DOWN BACK docker container"
    docker-compose -f $FILE down
    
    echo "UP BACK docker container"
    docker-compose -f $FILE up -d

    echo "Clean up building docker"
    docker rmi --force $(docker images -q --filter "dangling=true") || echo "test"

    echo "LOGS BACK"
    docker-compose -f $FILE logs -f --tail 100
}

# ADMIN
function admin () {
    echo "start admin app"

    echo "remove folder ./geo-service-admin"
    rm -Rf ./geo-service-admin

    echo "admin clone"
    git clone https://gitlab.com/zibellon-services/geo-service-admin.git

    echo "GS_WEB_ADMIN_BACK_URL = "$GS_WEB_ADMIN_BACK_URL

    #Сборка контейнера
    docker build ./geo-service-admin \
        --build-arg REACT_APP_BACK_URL=$GS_WEB_ADMIN_BACK_URL \
        -t geo-service-admin:latest

    echo "RM folder"
    rm -Rf ./geo-service-admin

    FILE=dc-admin.yaml

    echo "DOWN ADMIN docker container"
    docker-compose -f $FILE down
    
    echo "UP ADMIN docker container"
    docker-compose -f $FILE up -d

    echo "Clean up building docker"
    docker rmi --force $(docker images -q --filter "dangling=true") || echo "test"

    echo "LOGS ADMIN"
    docker-compose -f $FILE logs -f --tail 100
}

# Остановка всех контейнеров
function stopAll () {
    echo "stopAll"
    docker-compose -f dc-osm.yaml down
    docker-compose -f dc-photon.yaml down
    docker-compose -f dc-osrm.yaml down
    docker-compose -f dc-postgis.yaml down
    docker-compose -f dc-back.yaml down
    docker-compose -f dc-admin.yaml down
}

# Остановка всех контейнеров
function stopAndClear () {
    echo "stopAndClear"
    stopAll

    echo "remove all folders with data files"
    rm -Rf ./map-files
    rm -Rf ./osm-db
    rm -Rf ./photon-data
    rm -Rf ./postgis-db
}

# Запуск процесса обновления OSM
function osmUpdate () {
    echo "OSM UPDATE"
    docker exec -it geo-service-nominatim sudo -u nominatim nominatim replication --project-dir /nominatim
}

function photonUpdate () {
    echo "PHOTON UPDATE"
    curl http://localhost:$GS_PHOTON_PORT/nominatim-update
}

#################################

# $1 - CommandName
# $2 - argument

case $1 in
    map-data-custom-area)
        mapDataCustomArea
    ;;
    map-data-pbf-url)
        mapDataPbfUrl
    ;;
    osm)
        osm
    ;;
    photon-download)
        photonDownload
    ;;
    photon-import)
        photonImport
    ;;
    photon)
        photon
    ;;
    osrm-extract)
        osrmExtract
    ;;
    osrm)
        osrm
    ;;
    tile-server-import)
        tileServerImport
    ;;
    tile-server)
        tileServer
    ;;
    postgis)
        postgis
    ;;
    back)
        back
    ;;
    admin)
        admin
    ;;
    stop-all)
        stopAll
    ;;
    stop-and-clear)
        stopAndClear
    ;;
    photon-update)
        photonUpdate
    ;;
    osm-update)
        osmUpdate
    ;;
    *)
        echo "Invalid option, choose again..."
        exit 1
    ;;
esac